<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.Random, java.util.ArrayList, java.io.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP Directives</title>
</head>
<body>
<%@ include file="header.jsp" %>
	<h1>
		Random Number:
		<%
			Random r = new Random();
			int n = r.nextInt(60);
			//out.println(n);
		%>
		<%= n %>
	</h1>
</body>
</html>