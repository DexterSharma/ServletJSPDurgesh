<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="ad" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page errorPage="error.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Taglib jstl</title>
</head>
<body>
	<h1>Taglib</h1>
	<hr>
	<ad:set var="name" value="Aditya"></ad:set>
	<ad:out value="${name}"></ad:out>
	<ad:if test="${3>2 }">
		<h2>3 is greater than 2</h2>
	</ad:if>
	<%!
	int x = 50;
	int y = 20;
	String st = null;
	%>
	<% int di = x/y; %>
	<h1>Division = <%=di %></h1>
	<%= st.length() %>
</body>
</html>