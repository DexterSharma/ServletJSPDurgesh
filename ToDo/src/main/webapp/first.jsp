<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>First JSP</title>
</head>
<body style="background: #ffee99;">
	<h1>Hello</h1>
	<%!
		int a = 15;
		int b = 12;
		String s = "Hello";
	
		public int sum(int x, int y) {
			return x + y;
		}
		public String reverseString (String str) {
			return  new StringBuffer().append(str).reverse().toString();
		}
	%>
	<%
		out.println(a);
		out.println(b);
		out.println("<br>Sum="+sum(a, b));
	%>
	<h1 style="color: blue; font-weight: bold;">String s = <%= s %></h1>
	<br>
	Reverse of s = <%= reverseString(s) %>
</body>
</html>