<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ADHeader</title>
</head>
<body style="margin: 0px;">
<div style="color: white;background: #4287f5;">
<h1 style="text-align: center;margin: 0px">Practicing JSP</h1>
<div style="text-align: right;">
<h2 style="display: inline;">Current Date:</h2> <%=new Date().toString() %> 
</div>
</div>
</body>
</html>