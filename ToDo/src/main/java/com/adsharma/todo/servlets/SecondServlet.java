package com.adsharma.todo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class SecondServlet extends GenericServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8921737817397990997L;

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		System.out.println("SecondServlet extends GenericServlet");
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		out.println("<h1>Seconded</h1>");
	}
}
