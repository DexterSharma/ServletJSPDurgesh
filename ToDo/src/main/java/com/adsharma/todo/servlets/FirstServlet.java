package com.adsharma.todo.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.*;

public class FirstServlet implements Servlet {
	//Lifecycle methods
	ServletConfig config;
	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config=config;
		System.out.println("creating object:.............");
	}
	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		System.out.println("Servicing..............");
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		out.println("<h1>Output from servlet</h1>");
		out.println("<h2>Today's date and time is " + new Date().toString() + "</h2>");
	}
	@Override
	public void destroy() {
		System.out.println("Destroying servlet object.................");
	}
	
	//non-lifecycle methods
	@Override
	public ServletConfig getServletConfig() {
		return config;
	}
	@Override
	public String getServletInfo() {
		return "Test servlet ToDo";
	}
}
