package com.adsharma.todo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3051848065545793108L;
	String ms = "<h1>MyServlet:</h1>";
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("Get method of MyServlet");
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		out.println(ms+"<h2>Get method</h2>");
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("Post method of MyServlet");
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		out.println(ms+"<h2>Post method</h2>");
	}
}
