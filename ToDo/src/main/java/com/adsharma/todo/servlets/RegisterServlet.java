package com.adsharma.todo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegisterServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8258291181221856880L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		out.println("<h1>Welcome</h1>");
		String name = req.getParameter("user_name");
		String password = req.getParameter("user_password");
		String email = req.getParameter("user_email");
		String gender = req.getParameter("user_gender");
		String course = req.getParameter("user_course");
		String cond = req.getParameter("condition");
		
		if(cond!=null && cond.equals("checked")) {
			out.println("Name : "+name+"<br>");
			out.println("Password : "+password+"<br>");
			out.println("Email : "+email+"<br>");
			out.println("Gender : "+gender+"<br>");
			out.println("Course : "+course);
			req.getRequestDispatcher("success").forward(req, resp);
		} else {
			out.println("You have not accepted the terms and conditions");
			req.getRequestDispatcher("index.html").include(req, resp);
		}
	}
}
